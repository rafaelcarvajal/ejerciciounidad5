/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public class Triangulo extends Figura2D{
    
    private int base;
    private int altura;
    
    public Triangulo(){
        super();
    }
    
    public Triangulo(String nombre, int grosorBorde, int color, int base, int altura){
        super(nombre, grosorBorde, color);
        this.base = base;
        this.altura = altura;
    }
    
    public int getBase(){
        
        return this.base;
    }
    
    public void setBase(){
        
        this.base = base;
    }
    
    public int getAltura(){
        return this.altura;
    }
    
    public void setAltura(){
        this.altura = 1;
    }
    
    @Override
    public double calcularArea(){
        
        return 2.2;
    }
    
    @Override
    public double calcularPerimetro (){
        
        return 1.1;
    }
    
    /**
     *
     */
    @Override
    public void dibujar(){
        
    }
    
    public int cambiarTamanio(){
        
        return 1;
    }
    
    
}
