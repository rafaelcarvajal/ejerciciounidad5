/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public class Cuadrado extends Figura2D{
    
    private int lado;
    
    public Cuadrado(){
        
    }
    
    public Cuadrado(String nombre, int grosorBorde, int color, int lado){
        super(nombre, grosorBorde, color);
        this.lado = lado;
    }
    
    public int getLado(){
        return this.lado;
    }
    
    public void setLado(int lado){
        
        this.lado = lado;
    }
    
    @Override
    public double calcularPerimetro(){
        
        return 0;
        
    }
    
    @Override
    public void dibujar(){
        System.out.println("Se esta dibujando un Cuadrado");
    }
    
    public int cambiarTamanio(){
        
        return 0;
    }

    @Override
    public double calcularArea() {
        return Math.pow(this.lado, 2); 
    }
    
    
}
