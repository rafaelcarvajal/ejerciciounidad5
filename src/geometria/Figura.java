/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public abstract class  Figura {
    
    private String nombre;
    private int grosorBorde;
    private int color;
    
    
    public Figura(){
        
        nombre = "";
        grosorBorde = 0;
        color = 0;
        
    }
    
    public Figura(String nombre, int grosorBorde, int color){
        
        nombre = nombre;
        grosorBorde = grosorBorde;
        color = color;
    }
    
    public String getNombre(){
        
        return nombre;
    }
    
    public void setNombre(){
        this.nombre = nombre;
    }
    
    public int getGrosorBorde(){
        
        return grosorBorde;
    }
    
    public void setGrosorBorde(){
        
        this.grosorBorde = grosorBorde;
    }
    
    public int getColor(){
        return color;
    }
    
    public void setColor(){
        this.color = color;
    }
    
    public abstract void dibujar();
}
