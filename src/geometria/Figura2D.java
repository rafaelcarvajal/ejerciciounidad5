/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public abstract class Figura2D extends Figura {
    
    public Figura2D(String nombre, int grosorBorde, int color){
        
        super(nombre, grosorBorde, color);
    }
    
    public Figura2D(){
        super();
    }
    
    @Override
    public void dibujar(){
        
    }
    
    public abstract double calcularArea();
    
    public abstract double calcularPerimetro();
    
    
    
}
