/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public class Cubo extends Figura3D{
    
    private Cuadrado cuadrado;
    public Cubo(){
        super();
    }
    
    public Cubo(String nombre, int grosorBorde, int color, Cuadrado c){
        super(nombre, grosorBorde, color);
        this.cuadrado = c;
        
    }
    
    public Cuadrado getCuadrado(){
     
        return this.cuadrado;
    }
    
    
    public void setCuadrado(Cuadrado c){
        
        this.cuadrado = c;
        
    }
    
    
    public double calcularVolumen(){
        
        double lado = this.cuadrado.getLado();
        double result = Math.pow(lado,3);
        result = Math.pow(result, 3);
        return result;
    }
    
    public void dibujar(){
        System.out.println("Dibujando un cuadrado");
    }
    
}
