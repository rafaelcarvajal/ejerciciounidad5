/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

import geometria.Triangulo;
/**
 *
 * @author gyl
 */
public class Piramide extends Figura3D {
    
    private int altura;
    
    public Piramide(){
        
        altura = 0;
    }
    
    public Piramide(String nombre, int grosorBorde, int color, Triangulo t, int altura){
       
        super(nombre, grosorBorde, color);
        this.altura = altura;
        
    }
    
    public Triangulo getTriangulo(){
        
        Triangulo t = new Triangulo();
        
        return t;
    }
    
    public void setTriangulo(Triangulo t){
        
        
    }
    
    public int getAltura(){
        return 1;
    }
    
    public void setAltura(int in){
        
    }
    
    @Override
    public double calcularVolumen(){
        return 0;
        
    }
    
    @Override
    public void dibujar(){
        
    }
    
}
