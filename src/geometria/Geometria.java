/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public class Geometria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Cuadrado cuadrado1 = new Cuadrado("Cuadrado",1,1,5);
        
        System.out.println("El area del cuadrado de lado 2 es: " + cuadrado1.calcularArea());
        
        cuadrado1.dibujar();
        
        Cubo cubo1 = new Cubo("cubo", 1,1, cuadrado1);
        
        System.out.println("El volumen del cubo es: " + cubo1.calcularVolumen());
        
        cubo1.dibujar();
    }
    
}
