/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

/**
 *
 * @author gyl
 */
public abstract class Figura3D extends Figura{
    
    public Figura3D(){
        super();
    }
    
    public Figura3D(String nombre, int grosorBorde, int color){
        
        super(nombre, grosorBorde, color);
    }
    
    public abstract double calcularVolumen();
    
}
