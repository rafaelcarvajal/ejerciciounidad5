/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometria;

import java.util.ArrayList;

/**
 *
 * @author gyl
 */
public interface IColeccion {
    
    public boolean estaVacia();
    public ArrayList extraer();
    public Object primero();
    public boolean agregar(ArrayList<String> coleccion);
}
